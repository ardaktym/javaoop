package week1.homework;

public class Main {
    public static void main(String[] args) {
        Person p1 = new Person();
        p1.setName("Diana");
        p1.setBirthYear(2001);
        Person p2 = new Person("Milena",2000);
        Person p3 = new Person();
        p3.input("Zhasmina",2001);
        Person p4 = new Person("Venera", 2000);
        Person p5 = new Person();
        p5.setName("Sabina");
        p5.setBirthYear(2000);
        System.out.println(p4.age());
        System.out.println(p3.output());
        p3.changeName("Zhasya");
        System.out.println(p3.output());

    }
}
