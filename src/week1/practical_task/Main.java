package week1.practical_task;

public class Main {
    public static void main(String[] args) {
        Employee emp1 = new Employee();
        emp1.setHours(54);
        emp1.setName("Ardaktym");
        emp1.setRate(3500);
        Employee emp2 = new Employee("Bereke", 1259.4,48);
        Employee emp3 = new Employee("Arna", 2999.9);
        emp3.setHours(64);
        System.out.println(emp1.toString());
        System.out.println(emp1.salary());
        emp1.changeRate(3000);
        System.out.println(emp1.toString());
        System.out.println(emp1.salary());
        emp3.setHours(34);
        System.out.println("total hours of all workers: " + Employee.totalSum);
    }
}
