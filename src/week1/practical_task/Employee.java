package week1.practical_task;

public class Employee {
    private String name;
    private double rate;
    private int hours;
    static int totalSum = 0;
    public Employee(){
    }
    public Employee(String name, double rate){
        this.name = name;
        this.rate = rate;
    }
    public Employee(String name, double rate, int hours){
        this.name = name;
        this.rate = rate;
        this.hours = hours;
        totalSum = totalSum + hours;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        totalSum = totalSum + hours - this.hours;
        this.hours = hours;
    }
    public double salary(){
        return rate*hours;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", rate=" + rate +
                ", hours=" + hours +
                '}';
    }
    public void changeRate(double rate){
        this.rate = rate;
    }
    public double bonuses(){
        return salary()*0.1;
    }

}
