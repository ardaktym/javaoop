package week2.character;

public interface WeaponBehavior {
    void useWeapon();
}
