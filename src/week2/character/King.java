package week2.character;

public class King extends Character {
    public King(){
        System.out.println("I am a King");
        weaponBehavior = new KnifeBehavior();
    }

    @Override
    public void fight() {
        System.out.print("I am a King and ");
        weaponBehavior.useWeapon();
    }
}
