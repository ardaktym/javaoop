package week2.character;

public class Knight extends Character{
    public Knight(){
        System.out.println("I am a Knigt");
        weaponBehavior = new SwordBehavior();
    }

    @Override
    public void fight() {
        System.out.print("I am a Knight and ");
        weaponBehavior.useWeapon();
    }
}
