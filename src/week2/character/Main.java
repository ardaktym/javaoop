package week2.character;

public class Main {
    public static void main(String[] args) {
        King king = new King();
        king.fight();
        Knight knight = new Knight();
        knight.fight();
        Queen queen = new Queen();
        queen.fight();
        Troll troll = new Troll();
        troll.fight();
        king.setWeaponBehavior(new SwordBehavior());
        king.fight();

    }
}
