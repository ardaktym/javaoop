package week2.character;

public class Troll extends Character {
    public Troll(){
        System.out.println("I am a Troll");
        weaponBehavior = new AxeBehaior();
    }

    @Override
    public void fight() {
        System.out.print("I am a Troll and");
        weaponBehavior.useWeapon();
    }
}
