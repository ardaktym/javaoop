package week2.character;

public class Queen extends Character{
    public Queen(){
        System.out.println("I am a Queen");
        weaponBehavior = new BowAndArrowBehavior();
    }

    @Override
    public void fight() {
        System.out.print("I am a Queen and ");
        weaponBehavior.useWeapon();
    }
}
