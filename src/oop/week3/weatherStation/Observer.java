package oop.week3.weatherStation;

public interface Observer {
    public void update(float temperature, float humidity, float pressure);
}
