package oop.week3.weatherStation;

public class ForecastDisplay implements DisplayElement, Observer{
    private float pressure;
    private float previous = 0;
    private Subject weatherData;

    public ForecastDisplay(Subject weatherData) {
        this.weatherData = weatherData;
        weatherData.registerObserver(this);
    }

    @Override
    public void update(float temperature, float humidity, float pressure) {
        this.pressure = pressure;
        display();
    }

    @Override
    public void display() {
        if(previous == pressure){
            System.out.println("More of the same");
            previous = pressure;
        }
        else if(previous < pressure){
            System.out.println("Improving weather on the way!");
            previous = pressure;
        }
        else{
            System.out.println("Watch out for cooler, rainy weather");
            previous = pressure;
        }
    }


}
