package oop.week3.weatherStation;

public interface DisplayElement {
    public void display();
}
