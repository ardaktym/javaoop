package oop.factory;

public class Main {
    public static void main(String[] args) {
        Factory factory = new Factory();
        Clothe hat = factory.create("Hat");
        Clothe shoe = factory.create("Shoe");
        hat.wear();
        shoe.wear();
    }
}
