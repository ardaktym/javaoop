package oop.factory;

public class Factory {
    public Clothe create(String name){
        switch(name){
            case "Hat": return new Hat();
            case "Shoe": return new Shoe();
            default: return null;
        }
    }
}
