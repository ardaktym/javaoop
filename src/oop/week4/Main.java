package oop.week4;

public class Main {
    public static void main(String[] args) {
        Footballer midfielder = new Footballer("Mesut");
        midfielder.wearSocks();
        midfielder.putNumber();

        try {
            midfielder.play();
        } catch (SoccerIsNotReadyToPlay soccerIsNotReadyToPlay) {
            soccerIsNotReadyToPlay.printStackTrace();
        }


    }
}
