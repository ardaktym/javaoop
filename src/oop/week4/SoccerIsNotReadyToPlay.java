package oop.week4;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

public class SoccerIsNotReadyToPlay extends Exception {
    public SoccerIsNotReadyToPlay(String name) {
        super(name);

        PrintWriter out = null;
        try {
            FileOutputStream myFile = new FileOutputStream("file.txt");
            out = new PrintWriter(myFile);
            out.print(name);
            out.close();
        } catch (IOException e) {
            System.out.println("Don't write date to File");
        } finally {
            out.close();
        }

    }
}

