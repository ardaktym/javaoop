package oop.week4;

public class Footballer {
    private String name;
    private boolean hasNo = false;
    private boolean hasShinGuards = false;
    private boolean hasSocks = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void putNumber(){
        hasNo = true;
        System.out.println("I have a number");
    }

    public Footballer(String name) {
        this.name = name;
    }

    public void wearShinGuards(){
        hasShinGuards = true;
        System.out.println("I have shin guards");
    }

    public void wearSocks(){
        hasSocks = true;
        System.out.println("I have socks");
    }

    public void play() throws SoccerIsNotReadyToPlay{
        if (hasNo && hasShinGuards && hasSocks) {
            System.out.println(  name + " is ready to play!!");
        } else {
            throw new SoccerIsNotReadyToPlay (name + " is not ready to play!");
        }

    }

}
