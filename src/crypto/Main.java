package crypto;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input plaintext (from a to z without space) to encrypt it: ");
        String text = scanner.nextLine();
        encrypt(text);
        System.out.println("Input ciphertext to decrypt it: ");
        String ciph = scanner.nextLine();
        System.out.println("Also it's key: ");
        String key = scanner.nextLine();
        decrypt(ciph, key);
    }
    public static void encrypt(String text){
        char[] arrTxt = text.toCharArray();
        char[] arrKey = new char[arrTxt.length];
        char[] arrCiph = new char[arrTxt.length];
        Random  random = new Random();
        for(int i = 0; i < arrTxt.length; i++){
            arrKey[i] = (char)(random.nextInt(26)+ 'a');
            int num = (arrKey[i]- 97 + arrTxt[i]);
            if (num > 122){
                int x = num - 122;
                num = 96 + x;
            }
            arrCiph[i] = (char)(num);
        }
        System.out.println("Text: "+String.valueOf(arrTxt));
        System.out.println("Key: "+String.valueOf(arrKey));
        System.out.println("Cipher: "+String.valueOf(arrCiph));
    }
    public static void decrypt(String ciph, String key){
        char[] arrCiph = ciph.toCharArray();
        char[] arrKey = key.toCharArray();
        char[] arrTxt = new char[arrCiph.length];
        for(int i = 0; i< arrCiph.length; i++){
            int num = (arrCiph[i] - arrKey[i] + 97);
            if (num < 97){
                int x = 97 - num;
                num = 123 - x;
            }
            arrTxt[i] = (char)(num);
        }
        System.out.println("Ciph: "+ String.valueOf(arrCiph));
        System.out.println("Key: "+ String.valueOf(arrKey));
        System.out.println("Decrypted text: "+ String.valueOf(arrTxt));
    }

}
