package crypto.rsa;

import java.util.Random;
import java.util.Scanner;

import static java.lang.Math.pow;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input plaintext: ");
        String str = scanner.nextLine();
        char[] text = new char[str.length()];
        for (int i = 0; i < str.length(); i++) {
            text[i] = str.charAt(i);
        }
        int[] key = new int[str.length()];
        int p = 0;
        Random random = new Random();
        p = random.nextInt(100) + 1;
        while (!isPrime(p)) { // to choose only prime numbers
            p = random.nextInt(100) + 1;
        }
        int q = 0;
        q = random.nextInt(100) + 1;
        while (!isPrime(q) && q != p) {
            q = random.nextInt(100) + 1;
        }
        int n = p * q; // formula
        int phi = (p - 1) * (q - 1);
        int e = 0;
        e = random.nextInt(10)+1;
        while(!isPrime(e)){
            e = random.nextInt(10)+1;
        }

        int d = 0;
        while (e < phi) {
            if (gcd(e, phi) == 1)
                break;
            else
                e++;
        }
        for(int i  = 0 ; i < 10; i++){
            int x = 1 + (i*phi);
            if(x%e == 0){
                d = x/e;
                break;
            }
        }
        for(int i = 0; i < str.length(); i ++) { //convert to array of char key
            key[i] = text[i] - 96;
        }
        System.out.println("p= "+p +"  q= "+q+"  n= " +n+"  e= "+e+"  d= "+d);
        function(key,e,n,d);
    }
    private static void function(int[] key, int e, int n, int d){
        int[] ch1 = new int[key.length];
        int[] ch2 = new int[key.length];
        char[] plaintext = new char[ch2.length];
        for(int i = 0; i< key.length; i++){
            ch1[i]= encrypt(key[i],e,n);
        }


        for(int i = 0; i< ch1.length; i++){
            System.out.print(ch1[i]);
        }
        System.out.println();
        System.out.println("Encrypted data: " + java.util.Arrays.toString(ch1));
        //decryption
        for(int i = 0; i < ch2.length; i++){
            ch2[i] = decrypt(ch1[i], d, n);
        }
        for(int i = 0; i < ch2.length; i++){
            plaintext[i] = (char) (ch2[i] + 96 );
        }
        System.out.println("Decrypted plaintex: " + String.valueOf(plaintext));

    }

    private static int encrypt(int i, int e, int n){
        //ecnryption
        double c = pow(i,e);
        c = c % n;
        return (int) c;

    }

    private static int decrypt(int i, int d, int n){
        //decryption
        double m = emod(i, d, n);
        return (int) m;

    }


    private static double emod(double c, int d, int n) {
        if(d==0){//bcz program cant handle big numbers
            return 1;
        }
        else if(d%2==0){
            double f=emod(c,d/2,n);
            return (f*f)%n;
        }
        else{
            return ((c%n)*emod(c,d-1,n))%n;
        }
    }

    private static boolean isPrime(int inputNum) {
        if (inputNum <= 3 || inputNum % 2 == 0)
            return inputNum == 2 || inputNum == 3; //this returns false if number is <=1 & true if number = 2 or 3
        int divisor = 3;
        while ((divisor <= Math.sqrt(inputNum)) && (inputNum % divisor != 0))
            divisor += 2; //iterates through all possible divisors
        return inputNum % divisor != 0; //returns true/false
    }

    private static int gcd(int n1, int n2) {
        int gcd = 1;
        for (int i = 1; i <= n1 && i <= n2; ++i) {
            // Checks if i is factor of both integers
            if (n1 % i == 0 && n2 % i == 0)
                gcd = i;
        }
        return gcd;
    }
}



